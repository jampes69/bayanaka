<?php 
	$response = file_get_contents('http://test666999.herokuapp.com/all');
	$result = json_decode($response);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Bayanaka</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
	<div id="dynamic">
		<div class="row" id="row1" style="margin-top: 10px;">
			<div class="col-sm-6 col-sm-push-1">
				<select>
					<?php foreach ($result as $key => $value) : ?>
					<option value="tes"><?= $value->name ?></option>
					<?php endforeach ?>
				</select>
				<span> <button class="btn_remove" id="1" name="remove" style="color: red;">X</button></span>
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-6 col-sm-push-1">
			<button name="add" id="add" class="btn btn-danger">Add More</button>
		</div>
	</div>
</body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    	$(document).ready(function(){
        var i = 1;  
        

        $('#add').click(function(){  
        	i++;  
            $('#dynamic').append('<div class="row" id="row'+i+'" style="margin-top: 10px;"><div class="col-sm-6 col-sm-push-1"><select><?php foreach ($result as $key => $value) : ?>
					<option value="tes"><?= $value->name ?></option><?php endforeach ?></select><span> <button class="btn_remove" id="'+i+'" name="remove" style="color: red;">X</button></span></div></div>');  
        });  


        $(document).on('click', '.btn_remove', function(){  
            var button_id = $(this).attr("id"); 
            $('#row'+button_id+'').remove();  
        });
    });
    </script>
</html>